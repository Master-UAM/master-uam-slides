(TeX-add-style-hook
 "tfm_presentacion"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "presentation" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "latin1") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "inputenc"
    "fontenc"
    "fixltx2e"
    "graphicx"
    "longtable"
    "float"
    "wrapfig"
    "ulem"
    "textcomp"
    "marvosym"
    "wasysym"
    "latexsym"
    "amssymb"
    "amstext"
    "hyperref"
    "etoolbox"
    "fancybox"
    "tikz"
    "fancyvrb"
    "babel")
   (LaTeX-add-labels
    "sec-1"
    "sec-2"
    "sec-3"
    "sec-4"
    "sec-5"
    "sec-6")))

