\frametitle{Seccion 2}
\begin{block}{Getting help in R}
\begin{itemize}
\item Start html help, search/browse using web browser
\begin{itemize}
\item at the R console:
\begin{verbatim}
help.start()
\end{verbatim}

\item or use the help menu from you GUI
\end{itemize}

\item Look up the documentation for a function
\begin{verbatim}
help(topicName)
\end{verbatim}

\begin{verbatim}
?topicName
\end{verbatim}

\item Look up documentation for a package
\begin{verbatim}
help(package="packageName")
\end{verbatim}

\item Search documentation from R (not always the best way\ldots{} google often works better)
\begin{verbatim}
help.search("topicName")
\end{verbatim}
\end{itemize}
\end{block}
\begin{block}{Reading R documentation}
Beginners sometimes find the docs intimidating. It is much easier once you know the structure:
\begin{description}
\item[{Description}] What does this function do?
\item[{Usage}] Generic example showing default arguments
\item[{Arguments}] What are the inputs to this function?
\item[{Value}] What is the output (or result) of this function?
\item[{Details}] Additional details to help you understand how it works
\item[{References}] Citations for, e.g., the algorithms used in the function
\item[{See Also}] Other related functions
\item[{Examples}] Examples of the function in action; type example(functionName) to run them
\end{description}

In many cases just looking at the usage section is enough
\end{block}
\begin{block}{R packages and libraries}
There are thousands of R packages that extend R's capabilities. To get started, check out \url{http://cran.r-project.org/web/views/}.

\begin{itemize}
\item To view available packages:
\begin{verbatim}
library()
\end{verbatim}

\item To see what packages are loaded:
\begin{verbatim}
search()
\end{verbatim}

\item To load a package:
\begin{verbatim}
library("packageName")
\end{verbatim}

\item Install new package:
\begin{verbatim}
install.packages("packageName")
\end{verbatim}
\end{itemize}
\end{block}
\begin{block}{Things to keep in mind}
\begin{itemize}
\item Case sensitive, like Stata (unlike SAS)

\item Comments can be put almost anywhere, starting with a hash mark ('\texttt{\#}'); everything to the end of the line is a comment

\item The command prompt "\texttt{>}" indicates that R is ready to receive commands

\item If a command is not complete at the end of a line, R will give a different prompt, '\texttt{+}' by default

\item Parentheses must always match (first thing to check if you get an error)

\item R Does not care about spaces between commands or arguments

\item Names should start with a letter and should not contain spaces

\item Can use "." in object names (e.g., "my.data")

\item Use \alert{/} instead of \alert{\backslash} in path names
\end{itemize}
\end{block}
\appendix
\begin{enumerate}
\item Find the R console prompt and type '2+2 <enter>'
\item Look up the help page for the "mean" topic
\item Start the html help browser. From the browser, search for "linear model"
\item Use google and search for "R linear model". Click on the first link.
\item Go to \url{http://cran.r-project.org/web/views/} and skim the topic closest to your field/interests
\end{enumerate}
